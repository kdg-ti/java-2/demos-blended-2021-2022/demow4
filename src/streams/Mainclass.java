package streams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Mainclass {
    private ArrayList<Instrument> instruments;

    public static void main(String[] args) {
        new Mainclass();
    }

    public Mainclass()
    {
        initialiseInstruments();

        // Print all instrument names.
        // Iteratively
        System.out.println("\nInstrument names, iteratively:");
        for (Instrument instrument : instruments) {
                System.out.println(instrument.getName());
        }

        // Stream and filter
        System.out.println("\nInstrument names, using streams:");
        instruments.stream()                            // Wrapper to stream
            .map(instrument -> instrument.getName())    // Filter, intermediary operation
            .forEach(System.out::println);              // End operation

        // How many instruments cost more than 1000 Euro ?
        // Iteratively
        System.out.println("\nHow many instruments cost more than 1000 Euro ? Iteratively:");
        int numberOfInstruments = 0;
        for (Instrument instrument : instruments) {
            if (instrument.getPrice() > 1000) {
                numberOfInstruments++;
            }
        }
        System.out.println(numberOfInstruments);

        System.out.println("\nHow many instruments cost more than 1000 Euro ? Using streams:");
        // Stream and filter
        long nrOfArticles = instruments.stream()                    // Wrapper to stream
            .filter(instrument -> instrument.getPrice() > 1000)     // Filter, intermediary operation
            .count();                                               // End operation
        System.out.println(nrOfArticles);

        // Map a function on all items in a list.
        System.out.println("\nMap a function on all items in a list:");
        List<Integer> list1 = Arrays.asList(3, 6, 9, 12, 15);
        System.out.println("\nOriginal list");
        list1.stream()
            .forEach(System.out::println);
        System.out.println("\nMapped list");
        list1.stream()
            .map(number -> number * 3)
            .forEach(System.out::println);

        // Filter and map combined.
        // Run through instruments and print the names of instruments costing > 2000 Euro.
        System.out.println("\nFilter and map combined: the names of instruments costing > 2000 Euro.");
        instruments.stream()
            .filter(instrument -> instrument.getPrice() > 2000)
            .map(instrument -> instrument.getName())
            .forEach(System.out::println);

        // Run through instruments and store the names of instruments costing > 2000 Euro in a list of Strings.
        System.out.println("\nFilter and map combined: the names of instruments costing > 2000 Euro into a list of Strings.");
        List<String> names = instruments.stream()
                .filter(instrument -> instrument.getPrice() > 2000)
                .map(instrument -> instrument.getName())
                .collect(Collectors.toList());
        System.out.println(names);

        // Store all unique or distinct prices into a list of Integers.
        System.out.println("\nStore all unique or distinct prices into a list of Integers.");
        List<Integer> distinctPrices = instruments.stream()
                .map(instrument -> instrument.getPrice())
                .distinct()
                .collect(Collectors.toList());
        System.out.println(distinctPrices);

        // Exercises
        // Store all instrument names with an odd price in a list of Strings.
        System.out.println("\nStore all instruments with an odd price in a list of Strings.");

//        System.out.println(oddPrices);

        // Store all unique even instrument prices in a list of Integers.
        System.out.println("\nStore all unique even instrument prices in a list of Integers.");

//        System.out.println(evenPrices);


    }

    private void initialiseInstruments() {
        instruments = new ArrayList<Instrument>();

        instruments.add(new Instrument("Fender Telecaster", 2500));
        instruments.add(new Instrument("Yamaha SG", 1800));
        instruments.add(new Instrument("Gibson SG", 1807));
        instruments.add(new Instrument("Squire Stratocaster", 253));
        instruments.add(new Instrument("Gibson Les Paul", 2500));

        System.out.println("Instruments :\t\t" + instruments);
    }

}
