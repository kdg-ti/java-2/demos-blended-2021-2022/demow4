package lambdas;

import java.util.Comparator;

public class StringLengthComparator implements Comparator<String> {

    public StringLengthComparator() {
        super();
    }

    public int compare(String string1, String string2) {
        return Integer.compare(string1.length(), string2.length());
    }
}
