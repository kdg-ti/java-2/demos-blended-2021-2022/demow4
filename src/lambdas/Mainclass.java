package lambdas;

import java.util.ArrayList;

public class Mainclass {
    private ArrayList<String> cars;
    private ArrayList<Human> humans;

    public static void main(String[] args) {
        new Mainclass();
    }

    public Mainclass()
    {
        initialiseCars();
        initialiseHumans();

        Lambdas lambdas = new Lambdas();

        lambdas.whatWeAreUsedToDo(cars, humans);
//        lambdas.theLambdaWay(cars, humans);
    }

    private void initialiseCars() {
        cars = new ArrayList<String>();

        cars.add("Volvo");
        cars.add("BMW");
        cars.add("Audi");
        cars.add("Mercedes");
        cars.add("Porsche");
        cars.add("Toyota");
        cars.add("Hyundai");
        cars.add("Mazda");
        cars.add("Renault");
        cars.add("Citroen");

        System.out.println("Cars :\t\t" + cars);
    }

    private void initialiseHumans()
    {
        humans = new ArrayList<Human>();

        humans.add(new Human("Sarah", 10));
        humans.add(new Human("Rafael", 25));
        humans.add(new Human("Eva", 17));
        humans.add(new Human("Barbara", 30));

        System.out.println("Humans :\t" + humans);
    }
}
