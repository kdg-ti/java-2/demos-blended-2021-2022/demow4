package lambdas;

public class Human implements Comparable {

    private String name;
    private int age;
    private SortType sortType;

    public Human(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void setSortType(SortType sortType)
    {
        this.sortType = sortType;
    }

    @Override
    public String toString() {
        return "lambdas.Human{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    public int compareTo(Object otherHuman) {
        if (sortType == SortType.NAME)
            return (this.getName().compareTo(((Human) otherHuman).getName()));
        if (sortType == SortType.AGE)
            return Integer.compare(this.getAge(), ((Human) otherHuman).getAge());

        return 0;
    }

    public String getName()             { return name; }
    public void setName(String name)    { this.name = name; }
    public int getAge()                 { return age; }
    public void setAge(int age)         { this.age = age; }

    public int getNameLength()          { return getName().length(); }
}
