package lambdas;

import java.util.*;

public class Lambdas {
    public void whatWeAreUsedToDo(ArrayList<String> cars, ArrayList<Human> humans) {

        System.out.println("\nWhat we are used to:");
        // Loop through collection and print items.
        System.out.println("\nPrinting:");
        for (int i = 0; i < cars.size(); i++)
            System.out.println(cars.get(i));

        // Change all strings to uppercase.
        System.out.println("\nChange all strings to uppercase:");
        for (int i = 0; i < cars.size(); i++)
            cars.set(i, cars.get(i).toUpperCase(Locale.ROOT));
        System.out.println(cars);

        // Get the longest carname.
        System.out.println("\nGet the longest carname:");
        int carLength = 0;
        String longestCar = "";

        for (int i = 0; i < cars.size(); i++)
            if (cars.get(i).length() > carLength) {
                longestCar = cars.get(i);
                carLength = cars.get(i).length();
            }
        System.out.println("longestCar = " + longestCar);

        // Sort the list alphabetically.
        System.out.println("\nSort the list alphabetically:");
        Collections.sort(cars);
        System.out.println("Sorted = " + cars);

        System.out.println("\nSort the list on string length: (We need a comparator...)");
        Collections.sort(cars, new StringLengthComparator());
        System.out.println("Sorted on string length = " + cars);

        // Sort the humans on name.
        System.out.println("\nSort the humans on name:");
        for (int i = 0; i < humans.size(); i++)
            humans.get(i).setSortType(SortType.NAME);
        Collections.sort(humans);
        System.out.println(humans);

        // Sort the humans on age.
        System.out.println("\nSort the humans on age:");
        for (int i = 0; i < humans.size(); i++)
            humans.get(i).setSortType(SortType.AGE);
        Collections.sort(humans);
        System.out.println(humans);

    }

    public void theLambdaWay(ArrayList<String> cars, ArrayList<Human> humans)
    {
        System.out.println("\nThe lambda and streaming way:");

        // Loop through collection and print items.
        System.out.println("\nPrinting:");
        cars.forEach(car -> System.out.println(car));

        // Change all strings to uppercase.
        System.out.println("\nChange all strings to uppercase:");
        cars.replaceAll(car -> car.toUpperCase(Locale.ROOT));
        System.out.println(cars);

        // Get the longest carname.
        System.out.println("\nGet the longest carname:");

        // 2 Possibilities

        // Method reference (slide 22)
//        Optional<String> longestCar = cars.stream().max(Comparator.comparingInt(String::length));
//        System.out.println(longestCar);

        String longestCar = Collections.max(cars, Comparator.comparing(car -> car.length()));
        System.out.println("longestCar = " + longestCar);

        // Sort the list alphabetically.
        System.out.println("\nSort the list alphabetically:");
        Collections.sort(cars);
        System.out.println("Sorted = " + cars);

        // Sort the list on length of string.
        System.out.println("\nSort the list on string length: using method reference");
        cars.sort(Comparator.comparingInt(String::length));
        System.out.println("Sorted on string length = " + cars);

        // Sort the humans on name.
        System.out.println("\nSort the humans on name:");
        humans.sort(Comparator.comparing(Human::getName));
        System.out.println("Humans sorted on name = " + humans);

        // Sort the humans on name length.
        System.out.println("\nSort the humans on name length:");
        humans.sort(Comparator.comparing(Human::getNameLength));
        System.out.println("Humans sorted on name = " + humans);

        // Sort the humans on age.
        System.out.println("\nSort the humans on age:");
        humans.sort(Comparator.comparing(Human::getAge));
        System.out.println("Humans sorted on age = " + humans);

        // Lambda Syntax

        // (args) -> { body }
        // () -> { body }
        // () -> single statement

    }
}
